#!/usr/bin/python

# Name of the application
APP_BASE_NAME = 'usurpa-api'

# File to update
USURPA_URL = 'https://usurpa.squat.net/usurpa.pdf'
USURPA_DOWNLOAD_PATH = 'usurpa.pdf'