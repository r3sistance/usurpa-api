import config

squatNameExceptions = config.NAME_EXCEPTIONS
zones = config.REGIONAL_ZONES


class Squat(object):
    def __init__(self, name=None):
        self.name = name
        self.rows = []

    def addRow(self, row):
        self.rows.append(row)

    @classmethod
    def isSquatName(cls, name):
        # TODO: the names that starts with caps and then minus (like L’ACABose) are not detected as squat name.
        # Maybe is possible to think a rule like if the first two letters are capital?
        if str.isupper(name) or squatNameExceptions in name:
            return name
        return False


class RegionalZone (object):

    def __init__(self, name=None):
        self.name = name
        self.rows = []
        self.squatList = []

    def addRow(self, row):
        """
        Add a row object [] into the sets of self.rows[]
        :param row:
        :return:
        """
        self.rows.append(row)

    # TODO: DRY this function is basically the same as parser.parseCSV
    def generateSquats(self):
        squat = None
        for row in self.rows:
            squatName = Squat.isSquatName(row[0])
            if squatName:
                squat = Squat(squatName)
                self.squatList.append(squat)
            if squat is not None:
                squat.addRow(row)

    # TODO: Try to do this without making loop
    # Check if a row contains a zone name defined on config file
    @classmethod
    def checkIfIsZone(cls, row):
        for zone in zones:
            if zone in row:
                return zone
        return False