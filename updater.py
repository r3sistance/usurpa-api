import sys, getopt, os, time
from datetime import datetime
import config
import urllib.request

appName = config.APP_BASE_NAME
usurpaURL = config.USURPA_URL
usurpaDownloadPath = config.USURPA_DOWNLOAD_PATH

def isUsurpadownloaded():
    return os.path.isfile(usurpaDownloadPath)

def getConnection(url = usurpaURL):
    return urllib.request.urlopen(url, timeout=30)

# Get last modified attribure of remote url
def getRemoteLastModified(conn):
    return datetime.strptime(conn.headers['last-modified'], '%a, %d %b %Y %H:%M:%S GMT')

# Get last modified of local file
def getLocalLastModified(file):
    if not  os.path.isfile(file):
        return False
    stat = os.stat(file)
    try:
        return datetime.fromtimestamp(stat.st_birthtime)
    except AttributeError:
        # We're probably on Linux. No easy way to get creation dates here,
        # so we'll settle for when its content was last modified.
        return datetime.fromtimestamp(stat.st_mtime)

def downloadUsurpa(url=usurpaURL, downloadPath = usurpaDownloadPath):
    print ("- Start download of ",  usurpaURL )
    # Download the file
    urllib.request.urlretrieve(url, downloadPath)
    # Set modification date
    t = time.mktime(getRemoteLastModified(getConnection()).timetuple())
    os.utime(usurpaDownloadPath, (t, t))

def isUsurpaUpdated(remote, local):
    if remote == local:
        return True
    return False

def updateUsurpa(output=usurpaDownloadPath):
    print("# ", appName,"check for updates ", )
    try:
        if not isUsurpadownloaded:
            print("- No ", appName," file found, proceed to download on\n", output)
            downloadUsurpa(usurpaURL, downloadPath=output)
        elif not isUsurpaUpdated(getRemoteLastModified(getConnection()), getLocalLastModified(output)):
            print("- ", appName," is not updated, proceed to update: \n", output)
            downloadUsurpa(usurpaURL, downloadPath=output)
            print(appName, "updated succesfully")
            print("Done.")
        else:
            print(appName, "was already updated")
    except:
        raise Exception("Error: Failed to update usurpa")
