import sys, getopt, os, time
import updater, config, parser


appName = config.APP_BASE_NAME

def fileExists(file):
    return os.path.isfile(file)

def updateUsurpa(output):
    if output is None:
        updater.updateUsurpa()
    else:
        updater.updateUsurpa(output)

def convertUsurpa(input=None, output=None):
    if input is None : input = config.USURPA_DOWNLOAD_PATH
    if output is None : output = config.USURPA_CSV

    if input is None or not fileExists(input):
        print ("Error: converting usurpa can't find ", input, 'or', output)
    elif output is None:
        print("Error:", output, "is None")
    else:
        parser.convertPdfToCsv(input=input, output=output)


def parseUsurpa(input=None):
    if input is None : input = config.USURPA_CSV

    if fileExists(input):
        parser.parseCSV(input=input)
    else:
        print ("Error: can't find", input)

# Main
def main(argv):
    try:
        def printBanner():
            f = open('banner.txt', 'r')
            file_contents = f.read()
            print( file_contents, end="", flush=True)
            print ("Welcome to ", appName, "!\n", end="", flush=True)
        def printHelp ():
            print("Help me to squat!!")
            print("\nExample:\n\tpython run.py -u -c -p\n\nOptions:")
            print("\t-h Print this help")
            print("\t-i Specify input file")
            print("\t-o Specify output file")
            print("\t-u Update", config.USURPA_DOWNLOAD_PATH, "from", config.USURPA_URL,
                  "\n\t   Specify output with -o")
            print("\t-c Convert", config.USURPA_DOWNLOAD_PATH, "to", config.USURPA_CSV,
                  "\n\t   Specify -i -o parameters to especify  input and output ")
            print("\t-p Parse", config.USURPA_CSV,". Specify -i to specify the input file")
            print("\t-❤️ Retrieve list of all empty houses to squat")

        printBanner()

        if argv.__len__() == 0:
            printHelp()
        try:
            opts, args = getopt.getopt(argv, "hucpi:o:", "parse=")  # Letters recognized. If a letter has argument follow it with a colon :, like p: fill be '-p arg'
        except getopt.GetoptError:
            printHelp()
            sys.exit(2)

        input = None
        output = None
        for opt, arg in opts:
            if opt == '-h':
                printHelp()
                # sys.exit(0)
                break
            elif opt in ("-i"):
                input = arg
            elif opt in ("-o"):
                output = arg
            elif opt in ("-u"):
                updateUsurpa(output)
            elif opt in ("-c"):
                convertUsurpa(input=input, output=output)
            elif opt in ("-p"):
                parseUsurpa(input=input)
            # elif opt in ("--parse"):
            #     file = None
            #     if arg != "":
            #         file=arg
            #     parseUsurpa(file=file)
            elif opt in ("-❤️"):
                printHelp()
    except:
        raise BaseException("Error: failed to execute the program")

if __name__ == '__main__':
    main(sys.argv[1:])