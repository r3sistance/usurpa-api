import tabula
import config
import csv
from models import RegionalZone

appName = config.APP_BASE_NAME
usurpaPdf = config.USURPA_DOWNLOAD_PATH
csvOut = config.USURPA_CSV

zoneList = []

def parseCSV(input=csvOut):

    print("# Parsig", input)
    print ("* Parsing areas")
    with open(input, 'r') as csvFile:
        reader = csv.reader(csvFile)
        regionalZone = None
        for row in reader:
            zoneName = RegionalZone.checkIfIsZone(row)
            # If is zone append it to the list of zones
            if zoneName:
                regionalZone = RegionalZone(zoneName)
                zoneList.append(regionalZone)
            if regionalZone is not None and not zoneName:
                regionalZone.addRow(row)

    csvFile.close()


    print ("* Creating squat objects")
    for zone in zoneList:
        zone.generateSquats()

    print("# Printing all")
    x = 0
    for zone in zoneList:
        print("--",zone.name, "squats:", zone.squatList.__len__())
        for squat in zone.squatList:
            print("\t",squat.name)
            for row in squat.rows:
                print("\t -- ",row.__len__(), row)

            x += 1

    print("Total squats found", x)
    print("Total areas found: ", zoneList.__len__())



def convertPdfToCsv(input=usurpaPdf, output=csvOut):
    print("# Converting", input, "to", output)
    df = tabula.convert_into(input, output, pages='all', multiple_tables=True, output_format="csv", stream=True)
    print("Done.")
    return output
